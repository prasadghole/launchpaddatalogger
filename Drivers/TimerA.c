/******************************************************************************
 * @file   	TimerA.c
 * @author  Prasad Ghole
 * @version
 * @brief
 *
 ******************************************************************************/

#include <msp430.h>

//*****************************************************************************
//
//! \brief Timer A in Upmode for 1 ms delay
//! \param
//! \return
//
//*****************************************************************************
void TimerA_Init(void)
{
	TA0CCTL0 = CCIE;		// CCR0 interrupt enabled
	TA0CCR0 = 8000;
	TA0CTL = TASSEL_2 + MC_1 + TACLR;    // SMCLK, upmode, clear TAR
}

