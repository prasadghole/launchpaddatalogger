/******************************************************************************
 * @file   	Clock.c
 * @author  Prasad Ghole
 * @version
 * @brief	Device driver for clock ID
 *
 ******************************************************************************/

#include <msp430.h>

//*****************************************************************************
//
//! \brief Copied from launch pad source code for 8 MHz SMCLK and MCLK
//! \param
//! \return
//
//*****************************************************************************
void Clock_Init(void)
{
	UCSCTL0 = 0x13D8;
	UCSCTL1 = 0x0050;
	UCSCTL2 = 0x10F3;
	UCSCTL3 = 0x0020;
	UCSCTL4 = 0x0244;
	UCSCTL5 = 0x0000;
	UCSCTL6 = 0x00CD;
	UCSCTL7 = 0x0402;
	UCSCTL8 = 0x0707;
}

