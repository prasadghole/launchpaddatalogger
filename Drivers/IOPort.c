/******************************************************************************
 * @file   	IOPort.c
 * @author  Prasad Ghole
 * @version
 * @brief
 *
 ******************************************************************************/
#include <msp430.h>

//*****************************************************************************
//
//! \brief Initialize ports
//! \param
//! \return
//
//*****************************************************************************
void Port_Init(void)
{
	P1OUT = 0x00;
	P1DIR |= 0x01;		//Port 1.0 LED output
	P2OUT = 0x00;
	P2DIR = 0xFF;
	P3OUT = 0x00;
	P3DIR = 0xFF;
	P4OUT = 0x00;
	P4DIR = 0xFF;
	P5OUT = 0x00;
	P5DIR = 0xFF;
	P6OUT = 0x00;
	P6DIR = 0xFF;
	P7OUT = 0x00;
	P7DIR = 0xFF;
	P8OUT = 0x00;
	P8DIR = 0xFF;
}

