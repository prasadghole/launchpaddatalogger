#ifndef DRIVERS_ADC_H_
#define DRIVERS_ADC_H_


//*****************************************************************************
//
// Exported function
//
//*****************************************************************************
extern void ADC_Init(void);
extern void ADC_Setvalue(uint16_t v);
extern uint16_t ADC_Getvalue(void);

#endif /* DRIVERS_ADC_H_ */
