/******************************************************************************
 * @file   	TimerA.h
 * @author  Prasad Ghole
 * @version
 * @brief
 *
 ******************************************************************************/

#ifndef LAUNCHPADDATALOGGER_DRIVERS_TIMERA_H_
#define LAUNCHPADDATALOGGER_DRIVERS_TIMERA_H_

//*****************************************************************************
//
// Exported function
//
//*****************************************************************************
extern void TimerA_Init(void);

#endif /* LAUNCHPADDATALOGGER_DRIVERS_TIMERA_H_ */
