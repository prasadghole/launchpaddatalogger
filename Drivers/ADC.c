/******************************************************************************
 * @file   	ADC.c
 * @author  Prasad Ghole
 * @version
 * @brief
 *
 ******************************************************************************/


#include <msp430.h>
#include <stdint.h>
#include "ADC.h"

static uint16_t ADC_Value = 0;


//*****************************************************************************
//
//! \brief
//! \param
//! \return
//
//*****************************************************************************
void ADC_Init(void)
{
	  ADC12CTL0 = ADC12SHT02 + ADC12ON;         // Sampling time, ADC12 on
	  ADC12CTL1 = ADC12SHP;                     // Use sampling timer
	  ADC12IE = 0x01;                           // Enable interrupt
	  ADC12CTL0 |= ADC12ENC;
}


//*****************************************************************************
//
//! \brief  Setter ADC buffer
//! \param
//! \return
//
//*****************************************************************************
void ADC_Setvalue(uint16_t  v)
{
	ADC_Value =  v ;
}

//*****************************************************************************
//
//! \brief  Getter ADC buffer
//! \param
//! \return
//
//*****************************************************************************
uint16_t ADC_Getvalue(void)
{
	return ADC_Value;
}
