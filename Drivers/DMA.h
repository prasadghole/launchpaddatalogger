/******************************************************************************
 * @file   	DMA.h
 * @author  Prasad Ghole
 * @version
 * @brief
 *
 ******************************************************************************/

#ifndef LAUNCHPADDATALOGGER_DRIVERS_DMA_H_
#define LAUNCHPADDATALOGGER_DRIVERS_DMA_H_

//*****************************************************************************
//
//
//
//*****************************************************************************
extern void DMA_Init(void);
extern void DMA_Send(uint8_t * buf, uint16_t count);


#endif /* LAUNCHPADDATALOGGER_DRIVERS_DMA_H_ */
