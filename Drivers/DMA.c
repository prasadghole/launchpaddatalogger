/******************************************************************************
 * @file   	DMA.c
 * @author  Prasad Ghole
 * @version
 * @brief 	DMA driver
 *
 ******************************************************************************/
#include <msp430.h>
#include <stdint.h>
#include <intrinsics.h>

//*****************************************************************************
//
//! \brief
//! \param
//! \return
//
//*****************************************************************************
void DMA_Init(void)
{
	DMACTL0 = DMA0TSEL_21;		//Destination UART A1 Transmit buffer

	DMA0CTL = DMADT_0 + DMADSTINCR_0 +  DMASRCINCR_3 +  DMASBDB + DMALEVEL + DMAIE;

    __data16_write_addr( ( unsigned short ) &DMA0DA, ( unsigned long ) &UCA1TXBUF );
}

//*****************************************************************************
//
//! \brief Setup and start DMA transmission
//! \param
//! \return
//
//*****************************************************************************
void DMA_Send(uint8_t * buf , uint16_t count )
{
    __data16_write_addr((unsigned short) &DMA0SA,(unsigned long) buf);
    DMA0SZ = count  ;
    DMA0CTL  |= DMAEN;
}
