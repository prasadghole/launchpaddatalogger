/******************************************************************************
 * @file   	Clock.h
 * @author  Prasad Ghole
 * @version
 * @brief
 *
 ******************************************************************************/

#ifndef LAUNCHPADDATALOGGER_DRIVERS_CLOCK_H_
#define LAUNCHPADDATALOGGER_DRIVERS_CLOCK_H_

//*****************************************************************************
//
// Exported function
//
//*****************************************************************************
extern void Clock_Init(void);


#endif /* LAUNCHPADDATALOGGER_DRIVERS_CLOCK_H_ */
