/******************************************************************************
 * @file   	Uart_A1.h
 * @author  Prasad Ghole
 * @version
 * @brief
 *
 ******************************************************************************/

#ifndef LAUNCHPADDATALOGGER_DRIVERS_UART_A1_H_
#define LAUNCHPADDATALOGGER_DRIVERS_UART_A1_H_

//*****************************************************************************
//
// Macros and switches
//
//*****************************************************************************
#define BAUD_HIGH	0
#define BAUD_LOW	4

#define UCA1_OS   1    // 1 = oversampling mode, 0 = low-freq mode
#define UCA1_BRS  5    // Value of UCBRS field in UCA1MCTL register
#define UCA1_BRF  3    // Value of UCBRF field in UCA1MCTL register

//*****************************************************************************
//
// Exported function
//
//*****************************************************************************
extern void Uart_A1_Init(void);

#endif /* LAUNCHPADDATALOGGER_DRIVERS_UART_A1_H_ */
