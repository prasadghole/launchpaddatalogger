/******************************************************************************
 * @file   	IOPort.h
 * @author  Prasad Ghole
 * @version
 * @brief
 *
 ******************************************************************************/

#ifndef LAUNCHPADDATALOGGER_DRIVERS_IOPORT_H_
#define LAUNCHPADDATALOGGER_DRIVERS_IOPORT_H_

//*****************************************************************************
//
// Exported functions
//
//*****************************************************************************
extern void Port_Init(void);


#endif /* LAUNCHPADDATALOGGER_DRIVERS_IOPORT_H_ */
