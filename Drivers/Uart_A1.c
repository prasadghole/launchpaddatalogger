/******************************************************************************
 * @file   	Uart_A1.c
 * @author  Prasad Ghole
 * @version
 * @brief
 *
 ******************************************************************************/

#include <msp430.h>
#include "Uart_A1.h"

//*****************************************************************************
//
//! \brief UART A1 115200 baud
//! \param
//! \return
//
//*****************************************************************************
void Uart_A1_Init(void)
{

    UCA1CTL1 |= UCSWRST;        // Put the USCI state machine in reset
    UCA1CTL1 |= UCSSEL__SMCLK;  // Use SMCLK as the bit clock

    // Set the baudrate
    UCA1BR0 = BAUD_LOW;
    UCA1BR1 = BAUD_HIGH;
    UCA1MCTL = (UCA1_BRF << 4) | (UCA1_BRS << 1) | (UCA1_OS);

    P4SEL |= BIT4+BIT5;         // Configure these pins as TXD/RXD

    UCA1CTL1 &= ~UCSWRST;       // Take the USCI out of reset
    UCA1IE |= UCRXIE;           // Enable the RX interrupt.  Now, when bytes are
                                // rcv'ed, the USCI_A1 vector will be generated.
}


