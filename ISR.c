/******************************************************************************
 * @file   	ISR.c
 * @author  Prasad Ghole
 * @version
 * @brief 	All interrupt routiens must be here
 *
 ******************************************************************************/

#include <msp430.h>
#include <stdint.h>
#include <Globals.h>
#include <RS232.h>
#include <ADC.h>

//*****************************************************************************
//
// File specific macros
//
//*****************************************************************************
#define COUNT_1_S_LED	(1000)			/* Based on 1 ms timer */
#define INTERFRAME_DELAY_CNT	( 1000 )	/* Based on 1 ms timer */

//*****************************************************************************
//
//! \brief 1 MS timer ISR
//! \param
//! \return
//!	\Description For SMCLK 8MHz
//*****************************************************************************
#pragma vector=TIMER0_A0_VECTOR
__interrupt void TIMER0_A0_ISR(void)
{
	static uint16_t LED_Heartbeat_Cnt = COUNT_1_S_LED;

	if (gReply_Delay_Cnt)
	{
		gReply_Delay_Cnt--;
	}

	if (0 == --LED_Heartbeat_Cnt)
	{
		P1OUT ^= 0x01;
		ADC12CTL0 |= ADC12SC;                   // Start sampling/conversion
		LED_Heartbeat_Cnt = COUNT_1_S_LED;
	}
	if (gFrame_delay_cnt)
	{
		gFrame_delay_cnt--;
	}
}
//*****************************************************************************
//
//! \brief DMA ISR for sending reply packet
//! \param
//! \return
//
//*****************************************************************************
#pragma vector=DMA_VECTOR
__interrupt void DMA_ISR(void)
{
	switch (DMAIV)
	{
		case 0x02:
			DMA0CTL &= ~DMAEN;     //disableTransfers
		break;
	}
}

//*****************************************************************************
//
//! \brief	Receive character on UART
//! \param
//! \return
//
//*****************************************************************************
#pragma vector=USCI_A1_VECTOR
__interrupt void bcUartISR(void)
{
	//TODO add error check and flags
	gFrame_delay_cnt = INTERFRAME_DELAY_CNT;
	RS232_Rx_Put(&RS232_Buf, UCA1RXBUF);

}

//*****************************************************************************
//
//! \brief	ADC isr
//! \param
//! \return
//
//*****************************************************************************
#pragma vector = ADC12_VECTOR
__interrupt void ADC12_ISR(void)
{
  switch(__even_in_range(ADC12IV,34))
  {
  case  0: break;                           // Vector  0:  No interrupt
  case  2: break;                           // Vector  2:  ADC overflow
  case  4: break;                           // Vector  4:  ADC timing overflow
  case  6:                                  // Vector  6:  ADC12IFG0
	  ADC_Setvalue(ADC12MEM0) ;
  case  8: break;                           // Vector  8:  ADC12IFG1
  case 10: break;                           // Vector 10:  ADC12IFG2
  case 12: break;                           // Vector 12:  ADC12IFG3
  case 14: break;                           // Vector 14:  ADC12IFG4
  case 16: break;                           // Vector 16:  ADC12IFG5
  case 18: break;                           // Vector 18:  ADC12IFG6
  case 20: break;                           // Vector 20:  ADC12IFG7
  case 22: break;                           // Vector 22:  ADC12IFG8
  case 24: break;                           // Vector 24:  ADC12IFG9
  case 26: break;                           // Vector 26:  ADC12IFG10
  case 28: break;                           // Vector 28:  ADC12IFG11
  case 30: break;                           // Vector 30:  ADC12IFG12
  case 32: break;                           // Vector 32:  ADC12IFG13
  case 34: break;                           // Vector 34:  ADC12IFG14
  default: break;
  }
}
