################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Drivers/ADC.c \
../Drivers/Clock.c \
../Drivers/DMA.c \
../Drivers/IOPort.c \
../Drivers/TimerA.c \
../Drivers/Uart_A1.c 

OBJS += \
./Drivers/ADC.obj \
./Drivers/Clock.obj \
./Drivers/DMA.obj \
./Drivers/IOPort.obj \
./Drivers/TimerA.obj \
./Drivers/Uart_A1.obj 

C_DEPS += \
./Drivers/ADC.pp \
./Drivers/Clock.pp \
./Drivers/DMA.pp \
./Drivers/IOPort.pp \
./Drivers/TimerA.pp \
./Drivers/Uart_A1.pp 

C_DEPS__QUOTED += \
"Drivers\ADC.pp" \
"Drivers\Clock.pp" \
"Drivers\DMA.pp" \
"Drivers\IOPort.pp" \
"Drivers\TimerA.pp" \
"Drivers\Uart_A1.pp" 

OBJS__QUOTED += \
"Drivers\ADC.obj" \
"Drivers\Clock.obj" \
"Drivers\DMA.obj" \
"Drivers\IOPort.obj" \
"Drivers\TimerA.obj" \
"Drivers\Uart_A1.obj" 

C_SRCS__QUOTED += \
"../Drivers/ADC.c" \
"../Drivers/Clock.c" \
"../Drivers/DMA.c" \
"../Drivers/IOPort.c" \
"../Drivers/TimerA.c" \
"../Drivers/Uart_A1.c" 


