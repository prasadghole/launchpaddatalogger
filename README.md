# TI Lanchpad MSP430F5529 DataLogger

![Board](https://github.com/prasadghole/images/blob/a36b04d5143c75b1dd58ee39dfd6605a6dbc740e/Main_Setup.jpg)

## Read and plot ADC value trend
A python scripy on my another project [TrendPlotter](https://github.com/prasadghole/TrendPlotter) will sample 20 values of ADC count and plot using matplotlib.

A sample plot image is below
-------
![Plot](https://github.com/prasadghole/images/blob/ADC.png)
