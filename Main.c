/******************************************************************************
 * @file   	Main.c
 * @author  Prasad Ghole
 * @version
 * @brief 	Program as main entry point for data logger
 *
 ******************************************************************************/

#include <msp430.h>
#include <intrinsics.h>
#include <stdint.h>

#include <IOPort.h>
#include <Clock.h>
#include <Uart_A1.h>
#include <ADC.h>
#include <ProtoSlave.h>
#include <TimerA.h>
#include <RS232.h>
#include <CommandProcess.h>
#include <DMA.h>
#include <Parameter.h>

#define RS_BUFFER_SIZE		(5)
#define TX_BUFFER_SIZE		(20)

//*****************************************************************************
//
// Data structures and variables
//
//*****************************************************************************
uint16_t gReply_Delay_Cnt = 0;
uint16_t gFrame_delay_cnt = 0 ;

RS232_config_t RS232_Buf;
uint8_t RxBuffer[RS_BUFFER_SIZE];
uint8_t TxBuffer[TX_BUFFER_SIZE];

//*****************************************************************************
//
// Local functions
//
//*****************************************************************************
static void Command_Task(void);

//*****************************************************************************
//
// Local variables
//
//*****************************************************************************
static PROTO_Slave_Config_t Slave_Config;
static Proto_Packet_t protopacket;


void main(void)
{

	WDTCTL = WDTPW + WDTHOLD;		// First thing first

	Clock_Init();

	Port_Init();
	Uart_A1_Init();
	TimerA_Init();
	DMA_Init();
	ADC_Init();

	Parameter_Init();

	RS232_Init(&RS232_Buf, RxBuffer, RS_BUFFER_SIZE, TxBuffer, TX_BUFFER_SIZE);

	Proto_Slave_Init(&Slave_Config, MODULE_DEV_1, DMA_Send);

	__enable_interrupt();

	while (1)
	{
		Command_Task();
	}
}

//*****************************************************************************
//
//! \brief Process incoming packets
//
//! \param
//! \return
//*****************************************************************************
static void Command_Task(void)
{
	static uint8_t replyReady = 0 ;
	static uint8_t Proto_Frame_Recv = 1;

	if (RS232_Packet_Data_Availble(&RS232_Buf) == 1)
	{
		Proto_Frame_Recv = Proto_Slave_Parse(&Slave_Config, &protopacket,
				RS232_Rx_Get(&RS232_Buf), gFrame_delay_cnt );
	}
	if (0 == Proto_Frame_Recv)
	{
		Proto_Frame_Recv = 1;
		replyReady = CommandProcess(&protopacket);
	}

	if( replyReady && (0 == gReply_Delay_Cnt))
	{
		replyReady = 0;
		Proto_Slave_Send(&Slave_Config , &protopacket);
	}

}

//*****************************************************************************
//
//! \brief Must run this
//! \param
//! \return
//
//*****************************************************************************
int _system_pre_init(void)
{

	// Disable Watchdog timer to prevent reset during

	WDTCTL = WDTPW | WDTHOLD;

	return 1;
}

