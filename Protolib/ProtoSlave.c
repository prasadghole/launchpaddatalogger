/******************************************************************************
 * @file   PROTO_Slave.c
 * @author
 * @version
 * @date
 * @brief
 *
 ******************************************************************************
 */

#include <stdint.h>
#include "Protolib.h"
#include "ProtoSlave.h"

//*****************************************************************************
//
//! \brief
//! \param
//! \return
//
//*****************************************************************************
void Proto_Slave_Init(PROTO_Slave_Config_t * slave_config, uint8_t module_id,
		FPTR_V_U8P_U16 transmitfunptr)
{
	slave_config->Module_Id = module_id;
	slave_config->transmitfunptr = transmitfunptr;

	assert(slave_config->transmitfunptr);
}

//*****************************************************************************
//
//! \brief
//! \param
//! \return
//
//*****************************************************************************
void Proto_Slave_Send(PROTO_Slave_Config_t * slave_config,
		Proto_Packet_t *packet)
{
	uint16_t checksum = 0;
	uint16_t i, len;

	packet->Start_flag = PACKET_START_FLAG;
	packet->Module_Address = SLAVE_ADDRESS;

	if (0 == packet->Data_Size)
	{
		//Make sure you write zero always as we are not clearing the prev transmission buffer
		checksum = 0;
		packet->Datafield[0] = checksum;
		packet->Datafield[1] = checksum;
		len = PROTO_ZERO_DATA_BUFFER_LEN;
	} else
	{
		for (i = 0; i < packet->Data_Size; i++)
		{
			checksum += packet->Datafield[i];
		}
		packet->Datafield[i] = checksum;
		packet->Datafield[i + 1] = checksum >> 8;
		len = PROTO_ZERO_DATA_BUFFER_LEN + packet->Data_Size;
	}

	slave_config->transmitfunptr((uint8_t *) packet, len);

	return;
}

//TODO add return state type
uint8_t Proto_Slave_Parse(PROTO_Slave_Config_t *cfg,
		Proto_Packet_t *protopacket, uint8_t rcvbyte, uint8_t timeout_flag)
{
	static eproto_Parse_State eparserState = eprotostate_idle;
	static uint8_t bytecounter = 0; // For multibyte payload fields
	static uint16_t checksumcalculated = 0;
	static uint16_t checksumrecv = 0;

	int8_t retvalue = eparserState;

	if (0 == timeout_flag)
	{
		eparserState = eprotostate_idle;
	}

	switch (eparserState)
	{
		case eprotostate_idle:
			if (PACKET_START_FLAG == rcvbyte)
			{
				checksumcalculated = 0; // Reset prev acculamated
				checksumrecv = 0;
				eparserState = eprotostate_module_address;
			}
		break;
		case eprotostate_module_address:
			if (cfg->Module_Id == rcvbyte)
			{
				eparserState = eprotostate_command_id_low;
			} else
			{
				eparserState = eprotostate_idle; // Not my data go back to idle
			}
		break;
		case eprotostate_command_id_low:
			protopacket->commandid = (eCommand_ID) rcvbyte;
			eparserState = eprotostate_command_id_high;
		break;
		case eprotostate_command_id_high:
			protopacket->commandid |= (eCommand_ID) (rcvbyte << 8);
			eparserState = eprotostate_datasize_low;
		break;
		case eprotostate_datasize_low:
			protopacket->Data_Size = rcvbyte;
			eparserState = eprotostate_datasize_high;
		break;
		case eprotostate_datasize_high:
			protopacket->Data_Size = protopacket->Data_Size | (rcvbyte << 8);
			if (0 == protopacket->Data_Size)
			{
				eparserState = eprotostate_checksum;
			} else
			{
				eparserState = eprotostate_data;
				checksumcalculated = 0;
			}
			bytecounter = 0;
		break;
		case eprotostate_data:
			protopacket->Datafield[bytecounter] = rcvbyte;
			checksumcalculated += rcvbyte; //Calculate on the fly
			if (protopacket->Data_Size <= ++bytecounter)
			{
				eparserState = eprotostate_checksum;
				bytecounter = 0;
			}
		break;
		case eprotostate_checksum:
			checksumrecv = checksumrecv | (rcvbyte << (bytecounter * 8));
			if (2 == ++bytecounter)
			{
				if (checksumrecv == checksumcalculated)
				{
					eparserState = eprotostate_idle;
					retvalue = 0;
				} else
				{
					retvalue = PROTO_ERR_CHECKSUM;
					eparserState = eprotostate_idle;
				}
			}
		break;
		default:
			eparserState = eprotostate_idle;
		break;

	}

	return (retvalue);
}

