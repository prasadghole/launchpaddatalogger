/******************************************************************************
 * @file   	ProtoMaster.h
 * @author  Prasad Ghole
 * @version
 * @brief
 *
 ******************************************************************************/

#ifndef LAUNCHPADDATALOGGER_PROTOLIB_PROTOMASTER_H_
#define LAUNCHPADDATALOGGER_PROTOLIB_PROTOMASTER_H_

#include "Protolib.h"

//*****************************************************************************
//
//  ITB master configuration module
//
//*****************************************************************************
typedef struct
{
    uint8_t Module_Id;
    uint8_t Max_Slave;
    const uint8_t * crctab;
    FPTR_V_U8P_U16 fptr_uarttransmit ;
    FPTR_V_U8P_U16 fptr_DataLogging;
} Proto_Master_Config_t;


//*****************************************************************************
//
//  Exported functions
//
//*****************************************************************************
extern void Proto_Master_Init( Proto_Master_Config_t *master_config , uint8_t module_id , uint8_t max_slave ,
        const uint8_t *crctab ,FPTR_V_U8P_U16 fptr_uarttransmit,  FPTR_V_U8P_U16 fptr_DataLogging);
extern void Proto_Master_Send( Proto_Master_Config_t *master_config , Proto_Packet_t *packet );
extern int8_t Proto_Master_Parse_Recvdata( Proto_Master_Config_t *master_config , uint8_t rcvbyte );
extern void Proto_Master_Uplink_Send( Proto_Master_Config_t *master_config );


#endif /* LAUNCHPADDATALOGGER_PROTOLIB_PROTOMASTER_H_ */
