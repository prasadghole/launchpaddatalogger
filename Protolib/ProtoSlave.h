/******************************************************************************
 * @file   	ProtoSlave.h
 * @author  Prasad Ghole
 * @version
 * @brief
 *
 ******************************************************************************/

#ifndef LAUNCHPADDATALOGGER_PROTOLIB_PROTOSLAVE_H_
#define LAUNCHPADDATALOGGER_PROTOLIB_PROTOSLAVE_H_

#include <assert.h>
#include "Protolib.h"

//*****************************************************************************
//
//  Configuration parameters for slave
//
//*****************************************************************************
#define     SLAVE_ADDRESS           ( 0x01 )

//*****************************************************************************
//
// Slave configuration module
//
//*****************************************************************************
typedef struct {
    uint8_t Module_Id ;
    void ( * transmitfunptr ) ( uint8_t * buffer , uint16_t count );
} PROTO_Slave_Config_t ;

//*****************************************************************************
//
//  Exported functions
//
//*****************************************************************************
extern void Proto_Slave_Init(PROTO_Slave_Config_t * slave_config , uint8_t module_id , FPTR_V_U8P_U16 transmitfunptr );
extern void Proto_Slave_Send( PROTO_Slave_Config_t * slave_config , Proto_Packet_t *packet );
extern uint8_t Proto_Slave_Parse(PROTO_Slave_Config_t *cfg, Proto_Packet_t *protopacket ,uint8_t rcvbyte,uint8_t timeout_flag);

#endif /* LAUNCHPADDATALOGGER_PROTOLIB_PROTOSLAVE_H_ */
