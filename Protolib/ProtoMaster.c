/******************************************************************************
 * @file   Proto_Master.c
 * @author
 * @version
 * @date
 * @brief
 *
 ******************************************************************************
 */

#include <assert.h>
#include "ProtoLib.h"
#include "ProtoMaster.h"

//*****************************************************************************
//
//! \brief
//! \param
//! \return
//
//*****************************************************************************
void Proto_Master_Init(Proto_Master_Config_t *master_config, uint8_t module_id,
		uint8_t max_slave, const uint8_t *crctab,
		FPTR_V_U8P_U16 fptr_uarttransmit, FPTR_V_U8P_U16 fptr_DataLogging)
{
	master_config->Module_Id = module_id;
	master_config->Max_Slave = max_slave;
	master_config->fptr_uarttransmit = fptr_uarttransmit;
	master_config->fptr_DataLogging = fptr_DataLogging;
	master_config->crctab = crctab;

	assert(master_config->fptr_uarttransmit);
	assert(master_config->fptr_DataLogging);
}
//*****************************************************************************
//
//! \brief
//! \param
//! \return
//
//*****************************************************************************
void Proto_Master_Send(Proto_Master_Config_t *master_config,
		Proto_Packet_t *packet)
{
	uint16_t checksum = 0;
	uint16_t i, len;

	if (0 == packet->Data_Size)
	{
		//Make sure you write zero always as we are not clearing the prev transmission buffer
		checksum = 0;
		packet->Datafield[0] = checksum;
		packet->Datafield[1] = checksum;
		len = PROTO_ZERO_DATA_BUFFER_LEN;
	} else
	{
		for (i = 0; i < packet->Data_Size; i++)
		{
			checksum += packet->Datafield[i];
		}
		packet->Datafield[i] = checksum;
		packet->Datafield[i + 1] = checksum >> 8;
		len = PROTO_ZERO_DATA_BUFFER_LEN + packet->Data_Size;
	}

	master_config->fptr_uarttransmit((uint8_t *) packet, len);

	return;
}
//*****************************************************************************
//
//! \brief
//! \param
//! \return
//!     \b  Proto_ERR_CHECKSUM checksum is not matching
//!     \b  0 Parse complete
//!     \b  positive current state of parser
//*****************************************************************************
int8_t Proto_Master_Parse_Recvdata(Proto_Master_Config_t *master_config,
		uint8_t rcvbyte)
{
	static eproto_Parse_State parsetstate = eprotostate_idle;
	static uint8_t bytecounter = 0; // For multibyte payload fields
	static uint16_t checksumcalculated = 0;
	static uint16_t checksumrecv = 0;
	static uint8_t datalen = 0;

	int8_t retvalue = parsetstate;

	switch (parsetstate)
	{
		case eprotostate_idle:
			if ( PACKET_START_FLAG == rcvbyte)
			{
				checksumcalculated = 0; // Reset prev acculamated
				checksumrecv = 0;
				datalen = 0;
				parsetstate = eprotostate_module_address;
			}
		break;
		case eprotostate_module_address:
			parsetstate = eprotostate_command_id_low;
		break;
		case eprotostate_command_id_low:
			parsetstate = eprotostate_command_id_high;
		break;
		case eprotostate_command_id_high:
			//ignore high byte
			parsetstate = eprotostate_datasize_low;
		break;
		case eprotostate_datasize_low:
			datalen = rcvbyte;
			parsetstate = eprotostate_datasize_high;
		break;
		case eprotostate_datasize_high:
			if (0 == datalen)
			{
				parsetstate = eprotostate_checksum;
			} else
			{
				parsetstate = eprotostate_data;
			}
			bytecounter = 0;
		break;
		case eprotostate_data:
			checksumcalculated += rcvbyte; //Calculate on the fly
			if (datalen <= ++bytecounter)
			{
				parsetstate = eprotostate_checksum;
				bytecounter = 0;
			}

		break;
		case eprotostate_checksum:
			checksumrecv = checksumrecv | (rcvbyte << (bytecounter * 8));
			if (2 == ++bytecounter)
			{
				if (checksumrecv == checksumcalculated)
				{
					parsetstate = eprotostate_idle;
					retvalue = 0;  // Success now FO can send data
				} else
				{
					parsetstate = eprotostate_idle;
					retvalue = PROTO_ERR_CHECKSUM;
				}
			}
		break;
		default:
			parsetstate = eprotostate_idle;
		break;
	}

	return (retvalue);
}

