/******************************************************************************
 * @file   	Protolib.h
 * @author  Prasad Ghole
 * @version
 * @brief
 *
 ******************************************************************************/
#ifndef LAUNCHPADDATALOGGER_PROTOLIB_PROTOLIB_H_
#define LAUNCHPADDATALOGGER_PROTOLIB_PROTOLIB_H_


#include <stdint.h>



typedef void ( * FPTR_V_U8P_U16 ) (uint8_t * , uint16_t );

// Major 1 minor 0
#define         LIBV_VERSION        ( 0X0100 )


#define         MAX_DATA_FIELD_SIZE     ( 100 )           /* It includes crc also as checksum position will depend upon data length */


//*****************************************************************************
//
//  Enumaraion for command id by master
//
//*****************************************************************************
typedef enum
{
    ID_RESET = 0x0001,
    ID_READ_PARAMETERS ,
    ID_WRITE_PARAMETERS,
    ID_BAUD_HIGH  ,
    ID_BAUD_LOW,
	ID_READ_ADC
} eCommand_ID;

//*****************************************************************************
//
// enums for response ID by slave
//
//*****************************************************************************
typedef enum
{
    ID_ERR_GENERAL = 0x1000,
    ID_ACK = 0x0000,
    ID_PARAMETERS = 0x0040,
	ID_ADC_VAL = 0x0080,
} eResponse_ID;


//*****************************************************************************
//
//  state machine for parser
//
//*****************************************************************************
typedef enum
{
    eprotostate_idle = 1 ,        // Should be 1
    eprotostate_module_address,
    eprotostate_command_id_low,
    eprotostate_command_id_high,
    eprotostate_datasize_low,
    eprotostate_datasize_high,
    eprotostate_data,
    eprotostate_checksum,
} eproto_Parse_State;


//*****************************************************************************
//
// Packet format
//
//*****************************************************************************
typedef struct
{
    uint8_t Start_flag;
    uint8_t Module_Address;
    eCommand_ID commandid;
    uint16_t Data_Size;
    uint8_t Datafield[ MAX_DATA_FIELD_SIZE ];    // 2 Bytes for checksum
} Proto_Packet_t;


//*****************************************************************************
//
//  Configuraon constants
//
//*****************************************************************************
#define         MODULE_ADDRESS_BROADCAST        ( 0x00 )
#define         MODULE_DEV_1			        ( 0x01 )
#define         MODULE_DEV_2			        ( 0x02 )
#define         MODULE_DEV_3			        ( 0x03 )
#define         MODULE_DEV_4			        ( 0x04 )

#define         PACKET_START_FLAG          		( 0xAA )

#define         PROTO_ZERO_DATA_BUFFER_LEN        ( 8 )

#define         PROTO_ERR_CHECKSUM                ( -1 )


#endif /* LAUNCHPADDATALOGGER_PROTOLIB_PROTOLIB_H_ */
