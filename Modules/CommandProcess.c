/******************************************************************************
 * @file   	CommandProcess.c
 * @author  Prasad Ghole
 * @version
 * @brief
 *
 ******************************************************************************/

#include <assert.h>
#include <Protolib.h>
#include <Parameter.h>
#include <ADC.h>

//*****************************************************************************
//
// Local functions
//
//*****************************************************************************
static void Command_Reply_Packet(eResponse_ID command, Proto_Packet_t * packet,
		const uint8_t *data, const uint16_t data_size);

//*****************************************************************************
//
//! \brief Process and create reply
//! \param
//! \return
//
//*****************************************************************************
uint8_t CommandProcess(Proto_Packet_t * packet)
{
	uint8_t retval = 1;
	uint16_t adcval;

	eCommand_ID command = packet->commandid;

	switch (command)
	{
		case ID_RESET:
		break;

		case ID_READ_PARAMETERS:
			Command_Reply_Packet(ID_PARAMETERS, packet, Get_Paravalue(2), 10);
		break;
		case ID_WRITE_PARAMETERS:
		break;
		case ID_BAUD_HIGH:
		break;
		case ID_BAUD_LOW:
		break;
		case ID_READ_ADC:
			adcval = ADC_Getvalue();
			Command_Reply_Packet(ID_ADC_VAL, packet, (uint8_t *)&adcval, 2);
		break;
		default:
			retval = 0;
		break;
	}

	return retval;
}

//*****************************************************************************
//
//! \brief
//! \param
//! \return
//
//*****************************************************************************
static void Command_Reply_Packet(eResponse_ID command, Proto_Packet_t * packet,
		const uint8_t *data, const uint16_t data_size)
{
	uint8_t i;

	assert(data_size < MAX_DATA_FIELD_SIZE);

	packet->commandid = (eResponse_ID) command;
	packet->Data_Size = data_size;

	for (i = 0; i < data_size; i++)
	{
		packet->Datafield[i] = data[i];
	}
}
