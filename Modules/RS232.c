/******************************************************************************
 * @file   	RS232.c
 * @author  Prasad Ghole
 * @version
 * @brief
 *
 ******************************************************************************/
#include <stdint.h>
#include <assert.h>
#include <RS232.h>
#include <DMA.h>

//*****************************************************************************
//
//! \brief
//! \param
//! \return
//
//*****************************************************************************
void RS232_Init(RS232_config_t *cfg , uint8_t * rxbufptr , uint16_t sizerxbuf , uint8_t * txbufptr , uint16_t sizetxbuf )
{
	cfg->Rx_buffer.Buffer = rxbufptr;
	cfg->Rx_buffer.size = sizerxbuf;
	cfg->Tx_buffer.Buffer = txbufptr;
	cfg->Tx_buffer.size = sizetxbuf;


    cfg->Rx_buffer.Writeptr = 0 ;
    cfg->Rx_buffer.Readptr = 0 ;

    cfg->Tx_buffer.Count = 0 ;
    cfg->Tx_buffer.Dataptr = 0;

}

//*****************************************************************************
//
//! \brief
//! \param
//! \return
//
//*****************************************************************************
uint8_t RS232_Packet_Data_Availble( RS232_config_t *cfg )
{
    if ( cfg->Rx_buffer.Writeptr == cfg->Rx_buffer.Readptr )
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

//*****************************************************************************
//
//! \brief  Place data in receive buffer
//! \param
//! \return
//
//*****************************************************************************
void RS232_Rx_Put( RS232_config_t *cfg , uint8_t dt )
{
	cfg->Rx_buffer.Buffer[cfg->Rx_buffer.Writeptr] = dt;

	if(++cfg->Rx_buffer.Writeptr >= cfg->Rx_buffer.size )
	{
		cfg->Rx_buffer.Writeptr = 0 ;
	}
}

//*****************************************************************************
//
//! \brief Get data from receive buffer
//! \param
//! \return
//
//*****************************************************************************
uint8_t RS232_Rx_Get( RS232_config_t *cfg )
{
	uint8_t retval = cfg->Rx_buffer.Buffer[cfg->Rx_buffer.Readptr];


	if(++cfg->Rx_buffer.Readptr >= cfg->Rx_buffer.size )
	{
		cfg->Rx_buffer.Readptr = 0 ;
	}

	return retval;
}
