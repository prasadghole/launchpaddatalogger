/******************************************************************************
 * @file   	CommandProcess.h
 * @author  Prasad Ghole
 * @version
 * @brief
 *
 ******************************************************************************/

#ifndef COMMANDPROCESS_H_
#define COMMANDPROCESS_H_

#include <stdint.h>

//*****************************************************************************
//
//
//
//*****************************************************************************
extern uint8_t CommandProcess( Proto_Packet_t * packet);

#endif /* COMMANDPROCESS_H_ */
