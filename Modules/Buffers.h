/******************************************************************************
 * @file   	Buffers.h
 * @author  Prasad Ghole
 * @version
 * @brief 	Data structures for buffers
 *
 ******************************************************************************/

#ifndef MODULES_BUFFER_H_
#define MODULES_BUFFER_H_

//*****************************************************************************
//
// Data Structures
//
//*****************************************************************************
typedef struct
{
    uint8_t * Buffer;
    uint16_t size;
    uint8_t Writeptr;
    uint8_t Readptr;
} Circular_Buffer_t;


typedef struct
{
    uint8_t *Buffer;
    uint16_t size;
    uint8_t Dataptr;    // Current byte transfer pointer
    uint8_t Count;  // No of bytes to transmit
}TX_Buffer_t;

typedef struct {
    Circular_Buffer_t Rx_buffer;
    TX_Buffer_t Tx_buffer ;
}RS485_config_t ;



#endif /* MODULES_BUFFER_H_ */
