/******************************************************************************
 * @file   	Paramters.c
 * @author  Prasad Ghole
 * @version
 * @brief
 *
 ******************************************************************************/

#include <stdint.h>
#include <assert.h>
#include "Parameter.h"

//*****************************************************************************
//
// Local variables
//
//*****************************************************************************
static uint8_t Paramter_Buffer[PARAMTER_BUFFER_LEN];

//*****************************************************************************
//
//! \brief
//! \param
//! \return
//
//*****************************************************************************
void Parameter_Init(void)
{
	uint8_t i = 0;

	for (i = 0; i < PARAMTER_BUFFER_LEN; i++)
	{
		Paramter_Buffer[i] = 0xCC;	//For test
	}
}

//*****************************************************************************
//
//! \brief Return paramter block
//! \param
//! \return
//
//*****************************************************************************
uint8_t * Get_Paravalue(uint8_t parastart)
{
	assert(parastart < PARAMTER_BUFFER_LEN);

	return(&Paramter_Buffer[parastart] );
}

