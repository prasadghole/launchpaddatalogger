/******************************************************************************
 * @file   	Parameter.h
 * @author  Prasad Ghole
 * @version
 * @brief
 *
 ******************************************************************************/
#ifndef LAUNCHPADDATALOGGER_MODULES_PARAMETER_H_
#define LAUNCHPADDATALOGGER_MODULES_PARAMETER_H_

//*****************************************************************************
//
// Macros
//
//*****************************************************************************
#define		PARAMTER_BUFFER_LEN 	(128)

//*****************************************************************************
//
// Exported Functions
//
//*****************************************************************************
extern uint8_t * Get_Paravalue(uint8_t parastart);
extern void Parameter_Init(void);

#endif /* LAUNCHPADDATALOGGER_MODULES_PARAMETER_H_ */
