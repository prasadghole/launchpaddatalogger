/******************************************************************************
 * @file   	RS232.h
 * @author  Prasad Ghole
 * @version
 * @brief
 *
 ******************************************************************************/
#ifndef LAUNCHPADDATALOGGER_MODULES_RS232_H_
#define LAUNCHPADDATALOGGER_MODULES_RS232_H_

#include <Buffers.h>

//*****************************************************************************
//
// Data Structure
//
//*****************************************************************************
typedef struct {
    Circular_Buffer_t Rx_buffer;
    TX_Buffer_t Tx_buffer ;
}RS232_config_t ;


//*****************************************************************************
//
// Exported functions
//
//*****************************************************************************
extern void RS232_Init(RS232_config_t *cfg , uint8_t * rxbufptr , uint16_t sizerxbuf , uint8_t * txbufptr , uint16_t sizetxbuf );
extern uint8_t RS232_Packet_Data_Availble( RS232_config_t *cfg );
extern void RS232_Rx_Put( RS232_config_t *cfg , uint8_t dt );
extern uint8_t RS232_Rx_Get( RS232_config_t *cfg );
extern void RS232_Transmit( RS232_config_t * cfg , uint8_t * buf , uint16_t cnt );

#endif /* LAUNCHPADDATALOGGER_MODULES_RS232_H_ */
