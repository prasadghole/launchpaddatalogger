/******************************************************************************
 * @file   	Globals.h
 * @author  Prasad Ghole
 * @version
 * @brief 	All global shared variable must be exported here. We should have minimum of these.
 *
 ******************************************************************************/

#ifndef LAUNCHPADDATALOGGER_GLOBALS_H_
#define LAUNCHPADDATALOGGER_GLOBALS_H_

#include <RS232.h>


extern uint16_t gReply_Delay_Cnt;
extern uint16_t gFrame_delay_cnt;
extern RS232_config_t RS232_Buf;


#endif /* LAUNCHPADDATALOGGER_GLOBALS_H_ */
